using System.Collections.Generic;

namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class Trades : IKrakenEntity
    {
        public Trades()
        {
            TradeDatas = new List<TradeData>();
        }

        public string PairName { get; set; }

        public List<TradeData> TradeDatas { get; set; }
    }
}