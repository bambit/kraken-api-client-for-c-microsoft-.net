﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class Asset : IKrakenEntity
    {
        public string AssetName { get; set; }

        /// <summary>
        ///     alternate name (parameter: altname)
        /// </summary>
        public string AlternativeName { get; set; }

        /// <summary>
        ///     asset class (parameter: aclass)
        /// </summary>
        public string Aclass { get; set; }

        /// <summary>
        ///     scaling decimal places for record keeping (parameter: decimals)
        /// </summary>
        public decimal Decimals { get; set; }

        /// <summary>
        ///     scaling decimal places for output display (parameter: display_decimals)
        /// </summary>
        public decimal DisplayDecimals { get; set; }
    }
}