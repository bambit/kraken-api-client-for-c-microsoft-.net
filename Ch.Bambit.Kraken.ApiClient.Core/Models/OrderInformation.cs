﻿using System;

namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class OrderInformation : IKrakenEntity
    {
        public string Id { get; set; }

        /// <summary>
        ///     Parameter refid
        /// </summary>
        public string ReferralOrderTransactionId { get; set; }

        /// <summary>
        ///     Parameter refid
        /// </summary>
        public string UserReferenceId { get; set; }

        public OrderStatus OrderStaus { get; set; }

        /// <summary>
        ///     Parameter opentm
        /// </summary>
        public DateTime OpenTime { get; set; }

        /// <summary>
        ///     Parameter closetm
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        ///     Parameter expiretm
        /// </summary>
        public DateTime ExpiredTime { get; set; }

        public OrderDescription OrderDescription { get; set; }

        public decimal Volume { get; set; }

        public decimal VolumeExecuted { get; set; }

        public decimal Cost { get; set; }

        public decimal Fee { get; set; }

        public decimal Price { get; set; }

        public string Misc { get; set; }

        public string Oflags { get; set; }
    }
}