﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public enum OrderType
    {
        Market,
        Limit,
        StopLoss,
        TakeProfit,
        StopLossProfit,
        StopLossProfitLimit,
        StopLossLimit,
        TakeProfitLimit,
        TrailingStop,
        TrailingStopLimit,
        StopLossAndLimit,
        SettlePosition
    }
}