using System;

namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class TradeData
    {
        public decimal Price { get; set; }

        public decimal Volume { get; set; }

        public BuyOrSell Type { get; set; }

        public OrderType OrderType { get; set; }

        public DateTime Time { get; set; }

        public string Miscellaneous { get; set; }
    }
}