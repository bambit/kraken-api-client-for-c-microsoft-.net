﻿using System;

namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class OhlcData
    {
        public DateTime Time { get; set; }

        public decimal Open { get; set; }

        public decimal High { get; set; }

        public decimal Low { get; set; }

        public decimal Close { get; set; }

        public decimal Vwap { get; set; }

        public decimal Volume { get; set; }

        public decimal Count { get; set; }
    }
}