﻿using System;

namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class PriceAndVolumeAndTimestamp
    {
        public decimal Price { get; set; }

        public decimal Volume { get; set; }

        public DateTime Timestamp { get; set; }
    }
}