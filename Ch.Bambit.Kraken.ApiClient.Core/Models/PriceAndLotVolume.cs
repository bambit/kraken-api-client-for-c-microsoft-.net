﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class PriceAndLotVolume
    {
        public decimal Price { get; set; }

        public decimal LotVolume { get; set; }
    }
}