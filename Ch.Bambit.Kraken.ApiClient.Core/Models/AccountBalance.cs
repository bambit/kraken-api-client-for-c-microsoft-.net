﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class AccountBalance : IKrakenEntity
    {
        public string AssetName { get; set; }

        public decimal Balance { get; set; }
    }
}