﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class TradeBalance : IKrakenEntity
    {
        /// <summary>
        ///     Parameter eb = equivalent balance (combined balance of all currencies)
        /// </summary>
        public decimal EquivalentBalance { get; set; }

        /// <summary>
        ///     Parameter tb = trade balance (combined balance of all equity currencies)
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        ///     Parameter m = margin amount of open positions
        /// </summary>
        public decimal MarginAmount { get; set; }

        /// <summary>
        ///     Parameter n = unrealized net profit/loss of open positions
        /// </summary>
        public decimal UnrealizedNetProfitOrLoss { get; set; }

        /// <summary>
        ///     Parameter c = cost basis of open positions
        /// </summary>
        public decimal CostBasis { get; set; }

        /// <summary>
        ///     Parameter v = current floating valuation of open positions
        /// </summary>
        public decimal CurrentFloatingValuation { get; set; }

        /// <summary>
        ///     Parameter e = equity = trade balance + unrealized net profit/loss
        /// </summary>
        public decimal Equity { get; set; }

        /// <summary>
        ///     Parameter mf = free margin = equity - initial margin (maximum margin available to open new positions)
        /// </summary>
        public decimal FreeMargin { get; set; }

        /// <summary>
        ///     Parameter ml = margin level = (equity / initial margin) * 100
        /// </summary>
        public decimal MarginLevel { get; set; }
    }
}