﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class PriceAndLotVolumeAndWholeLotVolume : PriceAndLotVolume
    {
        public decimal WholeLotVolume { get; set; }
    }
}