﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public enum BuyOrSell
    {
        Buy,
        Sell
    }
}