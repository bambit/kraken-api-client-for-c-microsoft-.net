﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class VolumeAndPercentage
    {
        public decimal Volume { get; set; }

        public decimal PercentageFee { get; set; }
    }
}