﻿using System.Collections.Generic;

namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class MarketDepth : IKrakenEntity
    {
        public MarketDepth()
        {
            Asks = new List<PriceAndVolumeAndTimestamp>();
            Bids = new List<PriceAndVolumeAndTimestamp>();
        }

        public string PairName { get; set; }

        /// <summary>
        ///     ask side array of array entries(<price>, <volume>, <timestamp>) (parameter: asks)
        /// </summary>
        public List<PriceAndVolumeAndTimestamp> Asks { get; set; }

        /// <summary>
        ///     bid side array of array entries(<price>, <volume>, <timestamp>) (parameter: bids)
        /// </summary>
        public List<PriceAndVolumeAndTimestamp> Bids { get; set; }
    }
}