﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class TickerInformation : IKrakenEntity
    {
        public string PairName { get; set; }

        public PriceAndLotVolumeAndWholeLotVolume Ask { get; set; }

        public PriceAndLotVolumeAndWholeLotVolume Bid { get; set; }

        public PriceAndLotVolume LastTradeClosed { get; set; }

        public TodayAndLast24Hours Volume { get; set; }

        public TodayAndLast24Hours VolumeWeightedAveragePrice { get; set; }

        public TodayAndLast24Hours NumberOfTrades { get; set; }

        public TodayAndLast24Hours Low { get; set; }

        public TodayAndLast24Hours High { get; set; }

        public decimal TodaysOpeningPrice { get; set; }
    }
}