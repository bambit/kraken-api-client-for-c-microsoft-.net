namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class TodayAndLast24Hours
    {
        public decimal Today { get; set; }

        public decimal Last24Hours { get; set; }
    }
}