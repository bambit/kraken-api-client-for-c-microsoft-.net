﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public enum OrderStatus
    {
        Pending,
        Open,
        Closed,
        Canceled,
        Expired
    }
}