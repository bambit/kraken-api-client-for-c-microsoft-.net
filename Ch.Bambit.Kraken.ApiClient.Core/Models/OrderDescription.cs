namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class OrderDescription
    {
        public string PairName { get; set; }

        public BuyOrSell Type { get; set; }

        public OrderType OrderType { get; set; }

        public decimal Price { get; set; }

        public decimal Price2 { get; set; }

        public string Leverage { get; set; }

        public string Order { get; set; }
    }
}