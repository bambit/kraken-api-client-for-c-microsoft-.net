﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class KrakenEntityWithLast<T> where T : class, IKrakenEntity
    {
        public T Entity { get; set; }

        public string Last { get; set; }
    }
}