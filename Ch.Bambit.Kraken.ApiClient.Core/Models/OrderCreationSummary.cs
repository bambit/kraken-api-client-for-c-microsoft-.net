﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class OrderCreationSummary : IKrakenEntity
    {
        public string OrderDescription { get; set; }

        public string Close { get; set; }

        public string[] TransactionIds { get; set; }
    }
}