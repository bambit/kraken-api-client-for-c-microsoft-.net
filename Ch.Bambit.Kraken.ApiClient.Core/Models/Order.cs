﻿using System;

namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class Order
    {
        public string PairName { get; set; }

        public BuyOrSell Type { get; set; }

        public OrderType OrderType { get; set; }

        public decimal Price { get; set; }

        public decimal Price2 { get; set; }

        public decimal Volume { get; set; }

        public string Leverage { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? ExpireTime { get; set; }

        public string UserReferenceId { get; set; }

        public bool ValidateOnly { get; set; }
    }
}