﻿using System.Collections.Generic;

namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class AssetPair : IKrakenEntity
    {
        public AssetPair()
        {
            Fees = new List<VolumeAndPercentage>();
            FeesMaker = new List<VolumeAndPercentage>();
        }

        public string PairName { get; set; }

        /// <summary>
        ///     alternate pair name (parameter: altname)
        /// </summary>
        public string AlternativeName { get; set; }

        /// <summary>
        ///     asset class of base component (parameter: aclass_base)
        /// </summary>
        public string AclassBase { get; set; }

        /// <summary>
        ///     asset class of quote component (parameter: aclass_quote)
        /// </summary>
        public string AclassQuote { get; set; }

        /// <summary>
        ///     asset id of base component (parameter: base)
        /// </summary>
        public string Base { get; set; }

        /// <summary>
        ///     asset id of quote component (parameter: quote)
        /// </summary>
        public string Quote { get; set; }

        /// <summary>
        ///     volume lot size (Paramter: lot)
        /// </summary>
        public string Lot { get; set; }

        /// <summary>
        ///     scaling decimal places for pair (parameter: pair_decimal)
        /// </summary>
        public decimal PairDecimal { get; set; }

        /// <summary>
        ///     scaling decimal places for volume (parameter: lot_decimal)
        /// </summary>
        public decimal LotDecimals { get; set; }

        /// <summary>
        ///     amount to multiply lot volume by to get currency volume (parameter: lot_multiplier)
        /// </summary>
        public decimal LotMultiplier { get; set; }

        /// <summary>
        ///     array of leverage amounts available when buying (parameter: leverage_buy)
        /// </summary>
        public decimal[] LeverageBuy { get; set; }

        /// <summary>
        ///     volume discount currency (parameter: fee_volume_currency)
        /// </summary>
        public string FeeVolumeCurrency { get; set; }

        /// <summary>
        ///     margin call level (parameter: margin_call)
        /// </summary>
        public decimal MarginCall { get; set; }

        /// <summary>
        ///     stop-out/liquidation margin level (parameter: margin_stop )
        /// </summary>
        public decimal MarginStop { get; set; }

        public List<VolumeAndPercentage> Fees { get; set; }

        /// <summary>
        ///     array of leverage amounts available when selling (parameter: leverage_sell)
        /// </summary>
        public decimal[] LeverageSell { get; set; }

        /// <summary>
        ///     maker fee schedule array in [volume, percent fee] tuples (if on maker/taker) (parameter: fees_maker)
        /// </summary>
        public List<VolumeAndPercentage> FeesMaker { get; set; }
    }
}