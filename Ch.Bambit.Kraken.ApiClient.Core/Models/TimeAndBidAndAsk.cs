﻿using System;

namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class TimeAndBidAndAsk
    {
        public DateTime Time { get; set; }

        public decimal Bid { get; set; }

        public decimal Ask { get; set; }
    }
}