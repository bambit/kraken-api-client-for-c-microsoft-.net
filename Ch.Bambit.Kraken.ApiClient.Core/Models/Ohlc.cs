﻿using System.Collections.Generic;

namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class Ohlc : IKrakenEntity
    {
        public Ohlc()
        {
            OhlcDatas = new List<OhlcData>();
        }

        public string PairName { get; set; }

        public List<OhlcData> OhlcDatas { get; set; }
    }
}