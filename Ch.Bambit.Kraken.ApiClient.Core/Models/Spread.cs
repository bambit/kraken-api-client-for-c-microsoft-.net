﻿using System.Collections.Generic;

namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class Spread : IKrakenEntity
    {
        public Spread()
        {
            SpreadData = new List<TimeAndBidAndAsk>();
        }

        public string PairName { get; set; }

        public List<TimeAndBidAndAsk> SpreadData { get; set; }
    }
}