﻿namespace Ch.Bambit.Kraken.ApiClient.Core.Models
{
    public class OrderCancelSummary : IKrakenEntity
    {
        /// <summary>
        ///     Parameter count = number of orders canceled
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        ///     Parameter pending = if set, order(s) is/are pending cancellation
        /// </summary>
        public bool Pending { get; set; }
    }
}