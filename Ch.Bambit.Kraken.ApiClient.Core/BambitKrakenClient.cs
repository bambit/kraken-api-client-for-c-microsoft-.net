﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Ch.Bambit.Kraken.ApiClient.Core.Helpers;
using Ch.Bambit.Kraken.ApiClient.Core.KrakenApi;
using Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json;

namespace Ch.Bambit.Kraken.ApiClient.Core
{
    /*
        Copyright(c) 2017 bambit GmbH (bambit.ch)

        Permission is hereby granted, free of charge, to any person obtaining
        a copy of this software and associated documentation files (the
        "Software"), to deal in the Software without restriction, including
        without limitation the rights to use, copy, modify, merge, publish,
        distribute, sublicense, and/or sell copies of the Software, and to
        permit persons to whom the Software is furnished to do so, subject to
        the following conditions:

        The above copyright notice and this permission notice shall be
        included in all copies or substantial portions of the Software.

        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
        EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
        MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
        IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
        CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
        TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
        SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */
    public class BambitKrakenClient : IKrakenClient
    {
        private readonly Uri _endpointPublicMarketData;
        private readonly Uri _endpointPrivateData;

        private static readonly IEnumerable<JsonMediaTypeFormatter> TicketInformationFormatter = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new TickerInformationResponseConverter()
                    }
                }
            }
        };

        private static readonly IEnumerable<JsonMediaTypeFormatter> AssetFormatter = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new AssetsResponseConverter()
                    }
                }
            }
        };

        private static readonly IEnumerable<JsonMediaTypeFormatter> MarketDepthFormatter = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new MarketDepthResponseConverter()
                    }
                }
            }
        };

        private static readonly IEnumerable<JsonMediaTypeFormatter> AssetPairFormatter = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new AssetPairResponseConverter()
                    }
                }
            }
        };

        private static readonly IEnumerable<JsonMediaTypeFormatter> OpenOrderFormatter = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new OrderInformationResponseConverter()
                    }
                }
            }
        };

        private static readonly IEnumerable<JsonMediaTypeFormatter> OhlcFormatter = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new OhlcResponseConverterWithLast()
                    }
                }
            }
        };

        private static readonly IEnumerable<JsonMediaTypeFormatter> TradesFormatter = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new TradesResponseConverterWithLast()
                    }
                }
            }
        };

        private static readonly IEnumerable<JsonMediaTypeFormatter> SpreadFormatter = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new SpreadResponseConverterWithLast()
                    }
                }
            }
        };

        private static readonly IEnumerable<JsonMediaTypeFormatter> AccountBalanceFormater = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new AccountBalanceResponseConverter()
                    }
                }
            }
        };

        private static readonly IEnumerable<JsonMediaTypeFormatter> TradeBalanceFormatter = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new TradeBalanceResponseConverter()
                    }
                }
            }
        };

        private static readonly IEnumerable<JsonMediaTypeFormatter> OrderCreationSummaryFormatter = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new OrderCreationSummaryResponseConverter()
                    }
                }
            }
        };

        private static readonly IEnumerable<JsonMediaTypeFormatter> OrderCancelSummaryFormatter = new[]
        {
            new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    Converters = new List<JsonConverter>
                    {
                        new OrderCancelSummaryResponseConverter()
                    }
                }
            }
        };

        public BambitKrakenClient()
        {
            string url = ConfigurationManager.AppSettings["KrakenApiAddress"];
            if (string.IsNullOrEmpty(url))
            {
                throw new ConfigurationErrorsException("Cannot find required appSettings with Key KrakenApiAddress in your app.config.");
            }

            string versionAsString = ConfigurationManager.AppSettings["KrakenApiVersion"];
            if (string.IsNullOrEmpty(versionAsString))
            {
                throw new ConfigurationErrorsException("Cannot find required appSettings with Key KrakenApiVersion in your app.config.");
            }

            int version;
            bool versionIsANumber = int.TryParse(versionAsString, out version);
            if (!versionIsANumber)
            {
                throw new ConfigurationErrorsException("appSettings with Key KrakenApiVersion needs an Integer as Value. Please check your app.config.");
            }
            _endpointPublicMarketData = new Uri($"{url}/{version}/public/");
            _endpointPrivateData = new Uri($"{url}/{version}/private/");
        }

        private async Task<KrakenResponse<T>> QueryPublicEndpoint<T>(string method, IEnumerable<KeyValuePair<string, string>> urlParameters, IEnumerable<JsonMediaTypeFormatter> formatter) where T: class, IKrakenEntity
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = _endpointPublicMarketData;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                FormUrlEncodedContent content = new FormUrlEncodedContent(urlParameters);
                HttpResponseMessage response = await client.PostAsync(method, content);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<KrakenResponse<T>>(formatter);
                }
            }
            return null;
        }

        private async Task<KrakenResponse<T>> QueryPrivateEndpoint<T>(string method, IEnumerable<KeyValuePair<string, string>> urlParameters, IEnumerable<JsonMediaTypeFormatter> formatter) where T : class, IKrakenEntity
        {
            KrakenDelegatingHandler krakenDelegatingHandler = new KrakenDelegatingHandler();
            using (var client = new HttpClient(krakenDelegatingHandler))
            {
                client.BaseAddress = _endpointPrivateData;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                FormUrlEncodedContent content = new FormUrlEncodedContent(urlParameters);
                HttpResponseMessage response = await client.PostAsync(method, content);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<KrakenResponse<T>>(formatter);
                }
            }
            return null;
        }

        public async Task<List<TickerInformation>> GetTickerInformation(params string[] pairs)
        {
            if (pairs == null || !pairs.Any())
            {
                return null;
            }

            KeyValuePair<string, string> pairParamter = new KeyValuePair<string, string>("pair", string.Join(",", pairs));
            KrakenResponse<TickerInformation> krakenResponse = await QueryPublicEndpoint<TickerInformation>("Ticker", new[] { pairParamter }, TicketInformationFormatter);
            return krakenResponse.Result;
        }

        public async Task<List<AssetPair>> GetAssetPairs(params string[] pairs)
        {
            if (pairs == null || !pairs.Any())
            {
                return null;
            }

            KeyValuePair<string, string> pairParamter = new KeyValuePair<string, string>("pair", string.Join(",", pairs));
            KrakenResponse<AssetPair> krakenResponse = await QueryPublicEndpoint<AssetPair>("AssetPairs", new[] { pairParamter }, AssetPairFormatter);
            return krakenResponse.Result;
        }

        public async Task<List<Asset>> GetAssets()
        {
            KrakenResponse<Asset> krakenResponse = await QueryPublicEndpoint<Asset>("Assets", new KeyValuePair<string, string>[0], AssetFormatter);
            return krakenResponse.Result;
        }


        public async Task<List<Asset>> GetAssets(params string[] assets)
        {
            KeyValuePair<string, string> pairParamter = new KeyValuePair<string, string>("asset", string.Join(",", assets));
            KrakenResponse<Asset> krakenResponse = await QueryPublicEndpoint<Asset>("Assets", new[] { pairParamter }, AssetFormatter);
            return krakenResponse.Result;
        }

        public async Task<MarketDepth> GetOrderBook(string pair, int maximumNumberOfBidAndAsks = 10)
        {
            if (string.IsNullOrEmpty(pair))
            {
                return null;
            }

            KeyValuePair<string, string> pairParamter = new KeyValuePair<string, string>("pair", pair);
            KeyValuePair<string, string> countParameter = new KeyValuePair<string, string>("count", maximumNumberOfBidAndAsks.ToString());
            KrakenResponse<MarketDepth> krakenResponse = await QueryPublicEndpoint<MarketDepth>("Depth", new[] { pairParamter, countParameter }, MarketDepthFormatter);
            return krakenResponse.Result.Single();
        }

        public async Task<KrakenEntityWithLast<Ohlc>> GetOhlc(string pair, string since = "", int timeFrame = 1)
        {
            if (string.IsNullOrEmpty(pair))
            {
                return null;
            }

            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("pair", pair),
                new KeyValuePair<string, string>("interval", timeFrame.ToString())
            };

            if (!string.IsNullOrEmpty(since))
            {
                parameters.Add(new KeyValuePair<string, string>("since", since));
            }

            KrakenResponse<Ohlc> krakenResponse = await QueryPublicEndpoint<Ohlc>("OHLC", parameters.ToArray() , OhlcFormatter);
            KrakenEntityWithLast<Ohlc> result = new KrakenEntityWithLast<Ohlc>() {Entity = krakenResponse.Result.Single(), Last = krakenResponse.Last};
            return result;
        }

        public async Task<KrakenEntityWithLast<Trades>> GetRecentTrades(string pair, string since = "")
        {
            if (string.IsNullOrEmpty(pair))
            {
                return null;
            }

            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("pair", pair),
            };

            if (!string.IsNullOrEmpty(since))
            {
                parameters.Add(new KeyValuePair<string, string>("since", since));
            }

            KrakenResponse<Trades> krakenResponse = await QueryPublicEndpoint<Trades>("Trades", parameters.ToArray(), TradesFormatter);
            KrakenEntityWithLast<Trades> result = new KrakenEntityWithLast<Trades>() { Entity = krakenResponse.Result.Single(), Last = krakenResponse.Last };
            return result;
        }

        public async Task<KrakenEntityWithLast<Spread>> GetRecentSpreadData(string pair, string since = "")
        {
            if (string.IsNullOrEmpty(pair))
            {
                return null;
            }

            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("pair", pair),
            };

            if (!string.IsNullOrEmpty(since))
            {
                parameters.Add(new KeyValuePair<string, string>("since", since));
            }

            KrakenResponse<Spread> krakenResponse = await QueryPublicEndpoint<Spread>("Spread", parameters.ToArray(), SpreadFormatter);
            KrakenEntityWithLast<Spread> result = new KrakenEntityWithLast<Spread>() { Entity = krakenResponse.Result.Single(), Last = krakenResponse.Last };
            return result;
        }

        public async Task<List<OrderInformation>> GetOpenOrders(string userref = "")
        {
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();

            if (!string.IsNullOrEmpty(userref))
            {
                parameters.Add(new KeyValuePair<string, string>("userref",userref));
            }

            KrakenResponse<OrderInformation> krakenResponse = await QueryPrivateEndpoint<OrderInformation>("OpenOrders", parameters, OpenOrderFormatter);
            return krakenResponse.Result;
        }

        public async Task<List<OrderInformation>> GetClosedOrders(DateTime start, DateTime end, string userref = "")
        {
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("start", UnixTimestampHelper.ConvertToUnixTimestamp(start)),
                new KeyValuePair<string, string>("end", UnixTimestampHelper.ConvertToUnixTimestamp(end))
            };

            if (!string.IsNullOrEmpty(userref))
            {
                parameters.Add(new KeyValuePair<string, string>("userref", userref));
               
            }

            KrakenResponse<OrderInformation> krakenResponse = await QueryPrivateEndpoint<OrderInformation>("ClosedOrders", parameters, OpenOrderFormatter);
            return krakenResponse.Result;
        }

        public async Task<List<AccountBalance>> GetAccountBalance()
        {
            KrakenResponse<AccountBalance> krakenResponse = await QueryPrivateEndpoint<AccountBalance>("Balance", new KeyValuePair<string, string>[0], AccountBalanceFormater);
            return krakenResponse.Result;
        }

        public async Task<TradeBalance> GetTradeBalance(string asset="")
        {
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>();

            if (!string.IsNullOrEmpty(asset))
            {
                parameters.Add(new KeyValuePair<string, string>("asset", asset));
            }

            KrakenResponse<TradeBalance> krakenResponse = await QueryPrivateEndpoint<TradeBalance>("TradeBalance", parameters, TradeBalanceFormatter);
            return krakenResponse.Result.Single();
        }

        public async Task<OrderCreationSummary> CreateOrder(Order order)
        {
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("pair", order.PairName),
                new KeyValuePair<string, string>("type", EnumHelper.ConvertBuyOrSell(order.Type)),
                new KeyValuePair<string, string>("ordertype", EnumHelper.ConvertToOrderType(order.OrderType)),
                new KeyValuePair<string, string>("price", Convert.ToString(order.Price, CultureInfo.InvariantCulture)),
                new KeyValuePair<string, string>("volume", Convert.ToString(order.Volume, CultureInfo.InvariantCulture))
            };


            if (order.Price2 > 0)
            {
                parameters.Add(new KeyValuePair<string, string>("price2",
                    Convert.ToString(order.Price2, CultureInfo.InvariantCulture)));
            }

            if (!string.IsNullOrEmpty(order.Leverage))
            {
                parameters.Add(new KeyValuePair<string, string>("leverage", order.Leverage));
            }

            if (order.StartTime.HasValue)
            {
                parameters.Add(new KeyValuePair<string, string>("starttm",
                    UnixTimestampHelper.ConvertToUnixTimestamp(order.StartTime.Value)));
            }

            if (order.ExpireTime.HasValue)
            {
                parameters.Add(new KeyValuePair<string, string>("expiretm",
                    UnixTimestampHelper.ConvertToUnixTimestamp(order.ExpireTime.Value)));
            }

            if (!string.IsNullOrEmpty(order.UserReferenceId))
            {
                parameters.Add(new KeyValuePair<string, string>("userref", order.UserReferenceId));
            }

            if (order.ValidateOnly)
            {
                parameters.Add(new KeyValuePair<string, string>("validate", Convert.ToString("true")));
            }


            KrakenResponse<OrderCreationSummary> krakenResponse = await QueryPrivateEndpoint<OrderCreationSummary>("AddOrder", parameters, OrderCreationSummaryFormatter);
            return krakenResponse.Result.Single();
        }

        public async Task<OrderCancelSummary> CancelOrder(string transactionId)
        {
            List<KeyValuePair<string, string>> parameters = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("txid", transactionId)
            };

            KrakenResponse<OrderCancelSummary> krakenResponse = await QueryPrivateEndpoint<OrderCancelSummary>("CancelOrder", parameters, OrderCancelSummaryFormatter);
            return krakenResponse.Result.Single();
        }
    }
}
