﻿using System;
using System.Globalization;

namespace Ch.Bambit.Kraken.ApiClient.Core.Helpers
{
    public static class UnixTimestampHelper
    {
        public static DateTime ConvertToDateTime(string unixTimestamp)
        {
            double unixTimeStampAsDouble = Convert.ToDouble(unixTimestamp);
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStampAsDouble).ToLocalTime();
            return dtDateTime;
        }

        public static string ConvertToUnixTimestamp(DateTime dateTime)
        {
            return Convert.ToString(dateTime.Subtract(new DateTime(1970, 1, 1)).TotalSeconds, CultureInfo.InvariantCulture);
        }
    }
}
