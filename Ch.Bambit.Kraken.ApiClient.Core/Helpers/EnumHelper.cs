using System;
using Ch.Bambit.Kraken.ApiClient.Core.Models;

namespace Ch.Bambit.Kraken.ApiClient.Core.Helpers
{
    public static class EnumHelper
    {
        public static string ConvertBuyOrSell(BuyOrSell enumType)
        {
            return enumType == BuyOrSell.Buy ? "buy" : "sell";
        }

        public static string ConvertToOrderType(OrderType enumType)
        {
            switch (enumType)
            {
                case OrderType.Limit:
                    return "limit";
                case OrderType.Market:
                    return "market";
                case OrderType.SettlePosition:
                    return "settle-position";
                case OrderType.StopLoss:
                    return "stop-loss";
                case OrderType.StopLossAndLimit:
                    return "stop-loss-and-limit";
                case OrderType.StopLossLimit:
                    return "stop-loss-limit";
                case OrderType.StopLossProfit:
                    return "stop-loss-profit";
                case OrderType.StopLossProfitLimit:
                    return "stop-loss-profit-limit";
                case OrderType.TakeProfit:
                    return "take-profit";
                case OrderType.TakeProfitLimit:
                    return "take-profit-limit";
                case OrderType.TrailingStop:
                    return "trailing-stop";
                default:
                    return "trailing-stop-limit";

            }
        }


        public static BuyOrSell ConvertBuyOrSell(string buyOrSell)
        {
            if ("s".Equals(buyOrSell, StringComparison.InvariantCultureIgnoreCase) ||
                "sell".Equals(buyOrSell, StringComparison.InvariantCultureIgnoreCase))
            {
                return BuyOrSell.Sell;
            }

            return BuyOrSell.Buy;
        }

        public static OrderType ConvertToOrderType(string marketOrLimit)
        {
            if ("m".Equals(marketOrLimit, StringComparison.InvariantCultureIgnoreCase) ||
                "market".Equals(marketOrLimit, StringComparison.InvariantCultureIgnoreCase))
            {
                return OrderType.Market;
            }

            return OrderType.Limit;
        }
    }
}