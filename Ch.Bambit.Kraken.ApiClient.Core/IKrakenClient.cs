using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ch.Bambit.Kraken.ApiClient.Core.Models;

namespace Ch.Bambit.Kraken.ApiClient.Core
{
    public interface IKrakenClient
    {
        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/public/Assets and returns all assetinformations from kraken
        /// </summary>
        /// /// <returns>A List of Asset</returns>
        Task<List<Asset>> GetAssets();

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/public/Assets and returns all Assetinformations for a given list of assetkeys.
        /// </summary>
        /// <param name="assets">A list of list of assets to get info on (for Example "GNO")</param>
        /// <returns>A List of Asset</returns>
        Task<List<Asset>> GetAssets(params string[] assets);

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/public/AssetPairs and returns all tradable Asset pairs for a given list of asset pairs.
        /// </summary>
        /// <param name="pairs">A list of list of asset pairs to get info on (for Example "ETHEUR")</param>
        /// <returns></returns>
        Task<List<AssetPair>> GetAssetPairs(params string[] pairs);

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/public/Ticker and returns the tickerinfos for a given list of asset pairs.
        /// </summary>
        /// <param name="pairs">A list of list of asset pairs to get info on (for Example "ETHEUR")</param>
        /// <returns></returns>
        Task<List<TickerInformation>> GetTickerInformation(params string[] pairs);

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/public/OHLC and returns the OHLC Data for a given asset pair.
        /// </summary>
        /// <param name="pair">asset pair to get OHLC data for(for Example "ETHEUR")</param>
        /// <param name="since">Optional Return only Data since given id</param>
        /// <param name="timeFrame">Optional time frame interval in minutes (1 (default), 5, 15, 30, 60, 240, 1440, 10080, 21600)</param>
        /// <returns></returns>
        Task<KrakenEntityWithLast<Ohlc>> GetOhlc(string pair, string since = "", int timeFrame = 1);

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/public/Depth and returns Market Depth for a given asset pair.
        /// </summary>
        /// <param name="pair">asset pair to get OrderBook data for(for Example "ETHEUR")</param>
        /// <param name="maximumNumberOfBidAndAsks">Optional maximum number of asks/bids</param>
        /// <returns></returns>
        Task<MarketDepth> GetOrderBook(string pair, int maximumNumberOfBidAndAsks = 10);

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/public/Trades and returns the recent Trades for a given Asset Pair
        /// </summary>
        /// <param name="pair">asset pair to get trade data for (for Example "ETHEUR")</param>
        /// <param name="since">Optional Return only Data since given id</param>
        /// <returns></returns>
        Task<KrakenEntityWithLast<Trades>> GetRecentTrades(string pair, string since = "");

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/public/Spread and returns the spread data for a given Asset Pair
        /// </summary>
        /// <param name="pair">asset pair to get spread data for (for Example "ETHEUR")</param>
        /// <param name="since">Optional Return only Data since given id</param>
        /// <returns></returns>
        Task<KrakenEntityWithLast<Spread>> GetRecentSpreadData(string pair, string since = "");

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/private/OpenOrders and returns all information of a users open orders.
        /// </summary>
        /// <param name="userReferenceId">Optional restrict results to given user reference id (optional)</param>
        /// <returns></returns>
        Task<List<OrderInformation>> GetOpenOrders(string userReferenceId = "");

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/private/ClosedOrders and returns all information of a users closed orders.
        /// </summary>
        /// <param name="start">starting timestamp of results (exklusive)</param>
        /// <param name="end">ending timestamp of results (inclusive)</param>
        /// <param name="userReferenceId">Optional restrict results to given user reference id (optional)</param>
        /// <returns></returns>
        Task<List<OrderInformation>> GetClosedOrders(DateTime start, DateTime end, string userReferenceId = "");

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/private/Balance and returns all information of a users account balance
        /// </summary>
        /// <returns></returns>
        Task<List<AccountBalance>> GetAccountBalance();

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/private/TradeBalance and returns all information a users trading balance
        /// </summary>
        /// <param name="asset">Optional base asset used to determine balance (default = ZUSD)</param>
        /// <returns></returns>
        Task<TradeBalance> GetTradeBalance(string asset = "");

        /// <summary>
        /// Calls Endpoint https://www.kraken.com/help/api#cancel-open-order and adds a new Order
        /// </summary>
        /// <param name="order">The OrderSpecification</param>
        /// <returns></returns>
        Task<OrderCreationSummary> CreateOrder(Order order);

        /// <summary>
        /// Calls Endpoint https://api.kraken.com/0/private/CancelOrder and cancels an Order
        /// </summary>
        /// <param name="transactionId">The orderId</param>
        /// <returns></returns>
        Task<OrderCancelSummary> CancelOrder(string transactionId);
    }
}