using System.Collections.Generic;
using Ch.Bambit.Kraken.ApiClient.Core.Models;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi
{
    public class KrakenResponse<T> where T:class, IKrakenEntity
    {
        public KrakenResponse()
        {
            Result = new List<T>();
        }

        public bool HasError => Errors != null && Errors.Length > 0;

        public string[] Errors { get; set; }

        public List<T> Result { get; set; }

        public string Last { get; set; }
    }
}