﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Ch.Bambit.Kraken.ApiClient.Core
{
    public class KrakenDelegatingHandler : DelegatingHandler
    {
        private string _apiKey;
        private string _secret;

        public KrakenDelegatingHandler()
        {
            _apiKey = ConfigurationManager.AppSettings["KrakenKey"];
            if (string.IsNullOrEmpty(_apiKey))
            {
                throw new ConfigurationErrorsException("Cannot find required appSettings with Key KrakenKey in your app.config.");
            }

            _secret = ConfigurationManager.AppSettings["KrakenSecret"];
            if (string.IsNullOrEmpty(_secret))
            {
                throw new ConfigurationErrorsException("Cannot find required appSettings with Key KrakenSecret in your app.config.");
            }
            InnerHandler = new HttpClientHandler();
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            request.Headers.Add("API-Key", _apiKey);
            request.Headers.Add("API-Sign", CalculateApiSign(request, _secret).Result);
            return await base.SendAsync(request, cancellationToken);
        }

        /// <summary>
        /// Message signature using HMAC-SHA512 of (URI path + SHA256(nonce + POST data)) and base64 decoded secret API key
        /// http://bitoftech.net/2014/12/15/secure-asp-net-web-api-using-api-key-authentication-hmac-authentication/
        /// </summary>
        /// <param name="request"></param>
        /// <param name="secret"></param>
        /// <returns></returns>
        private static async Task<string> CalculateApiSign(HttpRequestMessage request, string secret)
        {
            Int64 nonce = DateTime.UtcNow.Ticks;
            string nonceWithOriginalPostData = "nonce=" + nonce;
            string content = await request.Content.ReadAsStringAsync();
            if (!string.IsNullOrEmpty(content))
            {
                nonceWithOriginalPostData = nonceWithOriginalPostData + "&" + content;
            }

            request.Content = CreateHttpContent(nonceWithOriginalPostData);

            byte[] key = Convert.FromBase64String(secret);
            byte[] path = Encoding.UTF8.GetBytes(request.RequestUri.LocalPath);

            // SHA256(nonce + POST data)
            string nonceAndContent = nonce + Convert.ToChar(0) + nonceWithOriginalPostData;
            byte[] hashedNonceAndContent;
            using (SHA256 hash = SHA256Managed.Create())
            {
                hashedNonceAndContent = hash.ComputeHash(Encoding.UTF8.GetBytes(nonceAndContent));
            }

            // HMAC-SHA512 of (URI path + SHA256(nonce + POST data))
            byte[] rawSignature = new byte[path.Count() + hashedNonceAndContent.Count()];
            path.CopyTo(rawSignature, 0);
            hashedNonceAndContent.CopyTo(rawSignature, path.Count());

            //Message signature using HMAC-SHA512 of (URI path + SHA256(nonce + POST data)) and base64 decoded secret API key
            using (HMACSHA512 hmacsha = new HMACSHA512(key))
            {
                return Convert.ToBase64String(hmacsha.ComputeHash(rawSignature));
            }
        }

        private static HttpContent CreateHttpContent(string postData)
        {
            NameValueCollection nameValueCollection = HttpUtility.ParseQueryString(postData);
            IEnumerable<KeyValuePair<string, string>> result = from string key in nameValueCollection select new KeyValuePair<string, string>(key, nameValueCollection[key]);
            return new FormUrlEncodedContent(result);
        }

    }
}