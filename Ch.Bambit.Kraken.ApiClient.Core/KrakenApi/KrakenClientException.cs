﻿using System;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi
{
    public class KrakenClientException : Exception
    {
        public KrakenClientException(string message) : base(message)
        {
            
        }
    }
}
