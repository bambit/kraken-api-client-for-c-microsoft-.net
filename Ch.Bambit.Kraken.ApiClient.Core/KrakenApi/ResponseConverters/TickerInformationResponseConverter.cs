using System;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class TickerInformationResponseConverter : KrakenResponseConverter<TickerInformation>
    {
        protected override TickerInformation ConvertResultToEntity(JProperty pair)
        {
            TickerInformation ticker = new TickerInformation {PairName = pair.Name};

            JArray aArray = (JArray)pair.Value["a"];
            JArray bArray = (JArray)pair.Value["b"];
            JArray cArray = (JArray)pair.Value["c"];
            JArray hArray = (JArray)pair.Value["h"];
            JArray vArray = (JArray)pair.Value["v"];
            JArray lArray = (JArray)pair.Value["l"];
            JArray tArray = (JArray)pair.Value["t"];
            JArray pArray = (JArray)pair.Value["p"];

            ticker.Ask = ConvertToPriceAndLotVolumeAndWholeLotVolume(aArray);
            ticker.Bid = ConvertToPriceAndLotVolumeAndWholeLotVolume(bArray);
            ticker.High = ConvertToTodayAndLast24Hours(hArray);
            ticker.LastTradeClosed = ConvertToPriceAndLotVolume(cArray);
            ticker.Low = ConvertToTodayAndLast24Hours(lArray);
            ticker.NumberOfTrades = ConvertToTodayAndLast24Hours(tArray);
            ticker.TodaysOpeningPrice = Convert.ToDecimal(pair.Value["o"]);
            ticker.Volume = ConvertToTodayAndLast24Hours(vArray);
            ticker.VolumeWeightedAveragePrice = ConvertToTodayAndLast24Hours(pArray);
            return ticker;
        }

        private static PriceAndLotVolumeAndWholeLotVolume ConvertToPriceAndLotVolumeAndWholeLotVolume(JArray jArray)
        {
            return new PriceAndLotVolumeAndWholeLotVolume() { Price = Convert.ToDecimal(jArray[0]), WholeLotVolume = Convert.ToDecimal(jArray[1]), LotVolume = Convert.ToDecimal(jArray[2]) };
        }

        private static TodayAndLast24Hours ConvertToTodayAndLast24Hours(JArray jArray)
        {
            return new TodayAndLast24Hours() { Today = Convert.ToDecimal(jArray[0]), Last24Hours = Convert.ToDecimal(jArray[1]) }; ;
        }

        private static PriceAndLotVolume ConvertToPriceAndLotVolume(JArray jArray)
        {
            return new PriceAndLotVolume() { Price = Convert.ToDecimal(jArray[0]), LotVolume = Convert.ToDecimal(jArray[1]) }; ;
        }

    }
}