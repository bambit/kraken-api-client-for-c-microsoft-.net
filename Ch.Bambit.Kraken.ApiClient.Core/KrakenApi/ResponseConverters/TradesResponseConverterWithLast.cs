using System;
using Ch.Bambit.Kraken.ApiClient.Core.Helpers;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class TradesResponseConverterWithLast : KrakenResponseConverterWithLast<Trades>
    {
        protected override Trades ConvertResultToEntity(JProperty pair)
        {
            Trades result = new Trades {PairName = pair.Name};
            foreach (JArray jArray in (JArray) pair.Value)
            {
                TradeData data = new TradeData
                {
                    Price = Convert.ToDecimal(jArray[0]),
                    Volume = Convert.ToDecimal(jArray[1]),
                    Time = UnixTimestampHelper.ConvertToDateTime(Convert.ToString(jArray[2])),
                    Type = EnumHelper.ConvertBuyOrSell(Convert.ToString(jArray[3])),
                    OrderType = EnumHelper.ConvertToOrderType(Convert.ToString(jArray[4])),
                    Miscellaneous = Convert.ToString(jArray[5])
                };

                result.TradeDatas.Add(data);
            }

            return result;
        }
    }
}