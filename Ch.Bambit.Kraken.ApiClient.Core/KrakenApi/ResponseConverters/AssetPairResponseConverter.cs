﻿using System;
using System.Collections.Generic;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class AssetPairResponseConverter : KrakenResponseConverter<AssetPair>
    {
        protected override AssetPair ConvertResultToEntity(JProperty pair)
        {
            AssetPair assetPair = new AssetPair { PairName = pair.Name };
            assetPair.AlternativeName = Convert.ToString(pair.Value["altname"]);
            assetPair.AclassBase = Convert.ToString(pair.Value["aclass_base"]);
            assetPair.Base = Convert.ToString(pair.Value["base"]);
            assetPair.AclassQuote = Convert.ToString(pair.Value["aclass_quote"]);
            assetPair.Quote = Convert.ToString(pair.Value["quote"]);
            assetPair.Lot = Convert.ToString(pair.Value["lot"]);
            assetPair.PairDecimal = Convert.ToDecimal(pair.Value["pair_decimals"]);
            assetPair.LotDecimals = Convert.ToDecimal(pair.Value["lot_decimals"]);
            assetPair.LotMultiplier = Convert.ToDecimal(pair.Value["lot_multiplier"]);
            assetPair.MarginCall = Convert.ToDecimal(pair.Value["margin_call"]);
            assetPair.MarginStop = Convert.ToDecimal(pair.Value["margin_stop"]);
            assetPair.FeeVolumeCurrency = Convert.ToString(pair.Value["fee_volume_currency"]);

            JArray leverageBuyArray = (JArray)pair.Value["leverage_buy"];
            JArray leveragSellArray = (JArray)pair.Value["leverage_sell"];
            JArray fees = (JArray) pair.Value["fees"];
            JArray feesMaker = (JArray)pair.Value["fees_maker"];

            assetPair.LeverageBuy = ConvertJArrayToDecimalArray(leverageBuyArray);
            assetPair.LeverageSell = ConvertJArrayToDecimalArray(leveragSellArray);
            assetPair.Fees = ConvertToVolumeAndPercentageList(fees);
            assetPair.FeesMaker = ConvertToVolumeAndPercentageList(feesMaker);

            return assetPair;
        }

        private static List<VolumeAndPercentage> ConvertToVolumeAndPercentageList(JArray jArray)
        {
            List<VolumeAndPercentage> result = new List<VolumeAndPercentage>();
            foreach (JToken jToken in jArray)
            {
                JArray valuesArray = (JArray) jToken;
                result.Add(new VolumeAndPercentage()
                {
                    PercentageFee = Convert.ToDecimal(valuesArray[0]),
                    Volume = Convert.ToDecimal(valuesArray[1])
                });

            }
            return result;
        }
    }
}
