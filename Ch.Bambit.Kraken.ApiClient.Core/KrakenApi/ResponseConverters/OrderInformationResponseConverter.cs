using System;
using System.Collections.Generic;
using System.Linq;
using Ch.Bambit.Kraken.ApiClient.Core.Helpers;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class OrderInformationResponseConverter : KrakenResponseConverter<OrderInformation>
    {
        protected override void TreatResultEntry(KrakenResponse<OrderInformation> root, JProperty pair)
        {
            if (pair.Name == "open" || pair.Name =="closed")
            {
                JObject openObject = pair.Children<JObject>().FirstOrDefault();
                if (openObject == null)
                {
                    throw new KrakenClientException("Result from Endpoint OpenOrder has unexpected format. Cannot parse this structure.");
                }

                IJEnumerable<JProperty> properties = openObject.Children<JProperty>();
                foreach (JProperty result in properties)
                {
                    root.Result.Add(ConvertResultToEntity(result));
                }
            }
        }

        protected override OrderInformation ConvertResultToEntity(JProperty pair)
        {
            OrderInformation result = new OrderInformation() { Id = pair.Name };

            result.ReferralOrderTransactionId = Convert.ToString(pair.Value["refid"]);
            result.UserReferenceId = Convert.ToString(pair.Value["userref"]);

            string orderStatus = Convert.ToString(pair.Value["status"]);
            switch (orderStatus.ToLowerInvariant())
            {
                case "pendending":
                    result.OrderStaus = OrderStatus.Pending;
                    break;
                case "open":
                    result.OrderStaus = OrderStatus.Open;
                    break;
                case "closed":
                    result.OrderStaus = OrderStatus.Closed;
                    break;
                case "canceled":
                    result.OrderStaus = OrderStatus.Canceled;
                    break;
                default:
                    result.OrderStaus = OrderStatus.Expired;
                    break;
            }
            result.OpenTime = UnixTimestampHelper.ConvertToDateTime(Convert.ToString(pair.Value["opentm"]));
            result.StartTime = UnixTimestampHelper.ConvertToDateTime(Convert.ToString(pair.Value["starttm"]));
            result.ExpiredTime = UnixTimestampHelper.ConvertToDateTime(Convert.ToString(pair.Value["expiretm"]));

            JToken descriptionObject = pair.Value["descr"];
            if (descriptionObject != null)
            {
                List<JProperty> orderDescriptionProperties = descriptionObject.Values<JProperty>().ToList();
                OrderDescription orderDescription =
                    new OrderDescription
                    {
                        PairName = Convert.ToString(orderDescriptionProperties[0].Value),
                        Price = Convert.ToDecimal(orderDescriptionProperties[3].Value),
                        Price2 = Convert.ToDecimal(orderDescriptionProperties[4].Value),
                        Leverage = Convert.ToString(orderDescriptionProperties[5].Value),
                        Order = Convert.ToString(orderDescriptionProperties[6].Value)
                    };

                string buyOrSell = Convert.ToString(orderDescriptionProperties[1].Value);
                string marketOrLimit = Convert.ToString(orderDescriptionProperties[2].Value);
                orderDescription.Type = EnumHelper.ConvertBuyOrSell(buyOrSell);
                orderDescription.OrderType = EnumHelper.ConvertToOrderType(marketOrLimit);
                result.OrderDescription = orderDescription;
            }

            result.Volume = Convert.ToDecimal(pair.Value["vol"]);
            result.VolumeExecuted = Convert.ToDecimal(pair.Value["vol_exec"]);
            result.Cost = Convert.ToDecimal(pair.Value["cost"]);
            result.Fee = Convert.ToDecimal(pair.Value["fee"]);
            result.Price = Convert.ToDecimal(pair.Value["price"]);
            result.Oflags = Convert.ToString(pair.Value["oflags"]);
            result.Misc = Convert.ToString(pair.Value["misc"]);

            return result;
        }
    }
}