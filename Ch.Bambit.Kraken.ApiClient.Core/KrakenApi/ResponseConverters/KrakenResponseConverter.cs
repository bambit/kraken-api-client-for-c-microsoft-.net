using System;
using System.Linq;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public abstract class KrakenResponseConverter<T> : JsonConverter where T:class, IKrakenEntity
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        protected abstract T ConvertResultToEntity(JProperty pair);

        protected virtual void TreatResultEntry(KrakenResponse<T> root, JProperty pair)
        {
            root.Result.Add(ConvertResultToEntity(pair));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);
            KrakenResponse<T> root = new KrakenResponse<T>();
            TreatError(jObject);
            TreatResult(jObject, root);
            return root;
        }

        protected virtual void TreatResult(JObject jObject, KrakenResponse<T> root)
        {
            JEnumerable<JProperty> results = jObject["result"].Children<JProperty>();
            foreach (JProperty result in results)
            {
                TreatResultEntry(root, result);
            }
        }

        protected static void TreatError(JObject jObject)
        {
            JArray errors = (JArray) jObject["error"];
            if (errors.Count > 0)
            {
                string message = string.Join(",", errors.Select(jToken => jToken.Value<string>()));
                throw new KrakenClientException(message);
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(KrakenResponse<T>));
        }

        public override bool CanWrite => false;

        protected static decimal[] ConvertJArrayToDecimalArray(JArray jArray)
        {
            decimal[] result = new decimal[jArray.Count];
            for (int i = 0; i < jArray.Count; i++)
            {
                result[i] = Convert.ToDecimal(jArray[i]);
            }

            return result;
        }

        protected static string[] ConvertJArrayToStringArray(JArray jArray)
        {
            string[] result = new string[jArray.Count];
            for (int i = 0; i < jArray.Count; i++)
            {
                result[i] = Convert.ToString(jArray[i]);
            }

            return result;
        }


    }
}