﻿using System;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class AccountBalanceResponseConverter : KrakenResponseConverter<AccountBalance>
    {
        protected override AccountBalance ConvertResultToEntity(JProperty pair)
        {
            return new AccountBalance {AssetName = pair.Name, Balance = Convert.ToDecimal(pair.Value)};
        }
    }
}