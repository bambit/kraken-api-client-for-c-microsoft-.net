using System;
using System.Collections.Generic;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class AssetsResponseConverter : KrakenResponseConverter<Asset>
    {
        protected override Asset ConvertResultToEntity(JProperty pair)
        {
            Asset asset = new Asset { AssetName = pair.Name };
            asset.AlternativeName = Convert.ToString(pair.Value["altname"]);
            asset.Aclass = Convert.ToString(pair.Value["aclass"]);
            asset.Decimals = Convert.ToDecimal(pair.Value["decimals"]);
            asset.DisplayDecimals = Convert.ToDecimal(pair.Value["display_decimals"]);
            return asset;
        }

        private static List<VolumeAndPercentage> ConvertToVolumeAndPercentageList(JArray jArray)
        {
            List<VolumeAndPercentage> result = new List<VolumeAndPercentage>();
            foreach (JToken jToken in jArray)
            {
                JArray valuesArray = (JArray)jToken;
                result.Add(new VolumeAndPercentage()
                {
                    PercentageFee = Convert.ToDecimal(valuesArray[0]),
                    Volume = Convert.ToDecimal(valuesArray[1])
                });

            }
            return result;
        }
    }
}