using System;
using System.Collections.Generic;
using Ch.Bambit.Kraken.ApiClient.Core.Helpers;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class MarketDepthResponseConverter : KrakenResponseConverter<MarketDepth>
    {
        protected override MarketDepth ConvertResultToEntity(JProperty pair)
        {
            MarketDepth result = new MarketDepth { PairName = pair.Name };

            JArray asksArray = (JArray)pair.Value["asks"];
            JArray bidsArray = (JArray)pair.Value["bids"];
            
            result.Asks = ConvertToPriceAndVolumeAndTimestampList(asksArray);
            result.Bids = ConvertToPriceAndVolumeAndTimestampList(bidsArray);

            return result;
        }

        private List<PriceAndVolumeAndTimestamp> ConvertToPriceAndVolumeAndTimestampList(JArray jArray)
        {
            List<PriceAndVolumeAndTimestamp> result = new List<PriceAndVolumeAndTimestamp>();
            foreach (JToken jToken in jArray)
            {
                JArray valuesArray = (JArray)jToken;
                result.Add(new PriceAndVolumeAndTimestamp()
                {
                    Price = Convert.ToDecimal(valuesArray[0]),
                    Volume = Convert.ToDecimal(valuesArray[1]),
                    Timestamp = UnixTimestampHelper.ConvertToDateTime(Convert.ToString(valuesArray[2]))
                });
            }
            return result;
        }
    }
}