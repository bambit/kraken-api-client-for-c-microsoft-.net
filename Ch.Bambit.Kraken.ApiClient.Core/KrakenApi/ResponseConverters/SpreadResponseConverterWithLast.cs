using System;
using Ch.Bambit.Kraken.ApiClient.Core.Helpers;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class SpreadResponseConverterWithLast : KrakenResponseConverterWithLast<Spread>
    {
        protected override Spread ConvertResultToEntity(JProperty pair)
        {
            Spread result = new Spread { PairName = pair.Name };
            foreach (JArray jArray in (JArray)pair.Value)
            {
                TimeAndBidAndAsk data = new TimeAndBidAndAsk
                {
                    Bid = Convert.ToDecimal(jArray[1]),
                    Ask = Convert.ToDecimal(jArray[2]),
                    Time = UnixTimestampHelper.ConvertToDateTime(Convert.ToString(jArray[0]))
                };
                result.SpreadData.Add(data);
            }

            return result;
        }
    }
}