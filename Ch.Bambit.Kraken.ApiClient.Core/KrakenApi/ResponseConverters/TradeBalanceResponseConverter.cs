using System;
using System.Collections.Generic;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class TradeBalanceResponseConverter : KrakenResponseConverter<TradeBalance>
    {
        protected override void TreatResult(JObject jObject, KrakenResponse<TradeBalance> root)
        {
            JObject resultObject = (JObject)jObject["result"];
            TradeBalance tradeBalance = new TradeBalance
            {
                EquivalentBalance = Convert.ToDecimal(resultObject["eb"]),
                Balance = Convert.ToDecimal(resultObject["tb"]),
                MarginAmount = Convert.ToDecimal(resultObject["m"]),
                UnrealizedNetProfitOrLoss = Convert.ToDecimal(resultObject["n"]),
                CostBasis = Convert.ToDecimal(resultObject["c"]),
                CurrentFloatingValuation = Convert.ToDecimal(resultObject["v"]),
                Equity = Convert.ToDecimal(resultObject["e"]),
                FreeMargin = Convert.ToDecimal(resultObject["mf"]),
                MarginLevel = Convert.ToDecimal(resultObject["ml"])
            };

            root.Result = new List<TradeBalance> {tradeBalance};
        }

        protected override TradeBalance ConvertResultToEntity(JProperty pair)
        {
            throw new NotImplementedException();
        }
    }
}