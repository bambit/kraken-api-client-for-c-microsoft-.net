using System;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class OrderCancelSummaryResponseConverter : KrakenResponseConverter<OrderCancelSummary>
    {
        protected override OrderCancelSummary ConvertResultToEntity(JProperty pair)
        {
            throw new NotImplementedException();
        }

        protected override void TreatResult(JObject jObject, KrakenResponse<OrderCancelSummary> root)
        {
            OrderCancelSummary orderCancelSummary = new OrderCancelSummary();
            JObject resultObject = (JObject)jObject["result"];
            orderCancelSummary.Count = Convert.ToInt32(resultObject["count"]);
            orderCancelSummary.Pending = !string.IsNullOrEmpty(Convert.ToString(resultObject["pending"]));
            root.Result.Add(orderCancelSummary);
        }
    }
}