using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public abstract class KrakenResponseConverterWithLast<T> : KrakenResponseConverter<T>
        where T : class, IKrakenEntity
    {
        protected override void TreatResultEntry(KrakenResponse<T> root, JProperty pair)
        {
            if (pair.Name == "last")
            {
                root.Last = pair.Value.ToString();
            }
            else
            {
                base.TreatResultEntry(root,pair);
            }
        }
    }
}