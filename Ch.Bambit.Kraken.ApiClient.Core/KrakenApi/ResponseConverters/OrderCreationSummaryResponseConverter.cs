using System;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class OrderCreationSummaryResponseConverter : KrakenResponseConverter<OrderCreationSummary>
    {
        protected override void TreatResult(JObject jObject, KrakenResponse<OrderCreationSummary> root)
        {
            OrderCreationSummary orderCreationSummary = new OrderCreationSummary();
            JObject resultObject = (JObject)jObject["result"];
            JObject description = (JObject) resultObject["descr"];
            orderCreationSummary.OrderDescription = Convert.ToString(description["order"]);
            orderCreationSummary.Close = Convert.ToString(description["close"]);
            orderCreationSummary.TransactionIds = ConvertJArrayToStringArray((JArray) resultObject["txid"]);
            root.Result.Add(orderCreationSummary);
        }

        protected override OrderCreationSummary ConvertResultToEntity(JProperty pair)
        {
           throw new NotImplementedException();
        }
    }
}