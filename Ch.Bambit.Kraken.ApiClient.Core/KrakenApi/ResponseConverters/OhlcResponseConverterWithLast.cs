using System;
using Ch.Bambit.Kraken.ApiClient.Core.Models;
using Newtonsoft.Json.Linq;

namespace Ch.Bambit.Kraken.ApiClient.Core.KrakenApi.ResponseConverters
{
    public class OhlcResponseConverterWithLast : KrakenResponseConverterWithLast<Ohlc>
    {
        protected override Ohlc ConvertResultToEntity(JProperty pair)
        {
            Ohlc result = new Ohlc { PairName = pair.Name };
            foreach (JArray jArray in (JArray)pair.Value)
            {
                OhlcData data = new OhlcData
                {
                    Open = Convert.ToDecimal(jArray[1]),
                    High = Convert.ToDecimal(jArray[2]),
                    Low = Convert.ToDecimal(jArray[3]),
                    Close = Convert.ToDecimal(jArray[4]),
                    Vwap = Convert.ToDecimal(jArray[5]),
                    Volume = Convert.ToDecimal(jArray[6]),
                    Count = Convert.ToDecimal(jArray[7])
                };

                result.OhlcDatas.Add(data);
            }

            return result;
        }
    }
}