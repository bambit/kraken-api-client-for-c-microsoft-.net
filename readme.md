# kraken 
This repository contains an Open Source implementation of a Kraken API Client for C# (Microsoft .NET) to access the Kraken REST API (https://www.kraken.com/).
[Kraken](https://www.kraken.com/) is a bitcoin (and other cryptocurrencies) exchange for serious and professional traders. 

# About this project
This project is provided by [bambit](https://bambit.ch). 

bambit transforms ideas into software. As a software company from Switzerland we build outstanding Software Solutions for our customers. Our mission is to enable our clients to accomplish and excel their visions in less time and effort. This means understanding their business as deeply as technology. 
We�re active in the field of E-Commerce, Online-Platforms and Custom Software Solutions. Our employees are long term E-Commerce, Software- and Microsoft-Specialists.You can expect a comprehensive range of services from our team and our network. 
Visit us at https://bambit.ch


# Usage

## Required Configuration
You need to add the following Entries within your app.config or web.config

``` XML
<appSettings>
	<add key="KrakenApiAddress" value="https://api.kraken.com" />
	<add key="KrakenApiVersion" value="0" />
	<add key="KrakenKey" value="<your Kraken API Key -> Key required to make queries to the API.>" />
	<add key="KrakenSecret" value="<your private Key -> Private key used to sign API messages.>" />
</appSettings>
```

## Sample Usage

``` CSharp
IKrakenClient client = new BambitKrakenClient();

while (true)
{
	List<TickerInformation> tickerInformations = await client.GetTickerInformation("GNOEUR", "BCHEUR");
	Console.WriteLine("------------------- {0} -------------------", DateTime.Now);
	foreach (TickerInformation tickerInformation in tickerInformations)
	{
		Console.WriteLine("{0}: Bid:{1}, Ask:{2}", tickerInformation.PairName, tickerInformation.Bid.Price,
			tickerInformation.Ask.Price);
	}
	Console.WriteLine();
	Thread.Sleep(300000);
}
```

## Available Methods
Those Methods are available within the Main-Interface IKrakenClient.

### Public Methods

| Method in IKrakenClient | Official Kraken Documentation |
| --------|---------|
| IKrakenClient.GetAssets(...); | https://www.kraken.com/help/api#get-asset-info |
| IKrakenClient.GetAssetPairs(...); | https://www.kraken.com/help/api#get-tradable-pairs | 
| IKrakenClient.GetTickerInformation(...); | https://www.kraken.com/help/api#get-ticker-info | 
| IKrakenClient.GetOhlc(...); | https://www.kraken.com/help/api#get-ohlc-data | 
| IKrakenClient.GetOrderBook(...); | https://www.kraken.com/help/api#get-order-book |
| IKrakenClient.GetRecentTrades(...); | https://www.kraken.com/help/api#get-recent-trades | 
| IKrakenClient.GetRecentSpreadData(...); | https://www.kraken.com/help/api#get-recent-spread-data | 

### Private User Data Methods

| Method in IKrakenClient | Official Kraken Documentation |
| --------|---------|
| IKrakenClient.GetOpenOrders(...); | https://www.kraken.com/help/api#get-open-orders |
| IKrakenClient.GetClosedOrders(...); | https://www.kraken.com/help/api#get-closed-orders | 
| IKrakenClient.GetAccountBalance(...); | https://www.kraken.com/help/api#get-account-balance | 
| IKrakenClient.GetTradeBalance(...); | https://www.kraken.com/help/api#get-trade-balance | 

### Private user trading Methods

| Method in IKrakenClient | Official Kraken Documentation |
| --------|---------|
| IKrakenClient.CreateOrder(...); | https://www.kraken.com/help/api#add-standard-order |
| IKrakenClient.CancelOrder(...); | https://www.kraken.com/help/api#cancel-open-order | 

# License
MIT License. See the LICENSE.txt file for license rights and limitations.

