﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Ch.Bambit.Kraken.ApiClient.Core;
using Ch.Bambit.Kraken.ApiClient.Core.Models;

namespace Ch.Bambit.Kraken.ApiClient.ConsoleAppication
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            MainAsync().Wait();
        }

        private static async Task MainAsync()
        {
            IKrakenClient client = new BambitKrakenClient();

            //List<Asset> allAssets = await client.GetAssets();

            //List<Asset> filteredAssets = await client.GetAssets("GNO", "ETH");
            //List<AssetPair> filteredAssetPairs = await client.GetAssetPairs("ETHEUR", "XXBTZEUR");

            //List<TickerInformation> tickerInformations = await client.GetTickerInformation("ETHEUR", "BCHEUR");

            //KrakenEntityWithLast<Ohlc> ohlcDatasWithLast = await client.GetOhlc("ETHEUR");
            //KrakenEntityWithLast<Ohlc> entityWithLast = await client.GetOhlc("ETHEUR", ohlcDatasWithLast.Last);

            //KrakenEntityWithLast<Trades> recendTradesWithLast = await client.GetRecentTrades("ETHEUR");
            //KrakenEntityWithLast<Trades> tradesSince = await client.GetRecentTrades("ETHEUR", recendTradesWithLast.Last);

            //KrakenEntityWithLast<Spread> spreadWithLast = await client.GetRecentSpreadData("ETHEUR");
            //KrakenEntityWithLast<Spread> spreadDataSince = await client.GetRecentSpreadData("ETHEUR", spreadWithLast.Last);


            //List<AccountBalance> accountBalances = await client.GetAccountBalance();
            //TradeBalance tradeBalance = await client.GetTradeBalance();
            //TradeBalance tradeBalanceInEuro = await client.GetTradeBalance("ZEUR");
            //List<OrderInformation> openOrders = await client.GetOpenOrders();
            //List <OrderInformation> closedOrders = await client.GetClosedOrders(DateTime.Now.AddDays(-20), DateTime.Now);

            //Order order = new Order()
            //{
            //    PairName = "ETHEUR",
            //    Price = 50,
            //    OrderType = OrderType.Limit,
            //    Type = BuyOrSell.Buy,
            //    Volume = 1,
            //    ValidateOnly = true // Order is only simulated as long ValidateOnly=true 
            //};
            //OrderCreationSummary orderCreationSummary = await client.CreateOrder(order);
            //OrderCancelSummary orderCancelSummary = await client.CancelOrder("XYZ");

            while (true)
            {
                List<TickerInformation> tickerInformations = await client.GetTickerInformation("ETHEUR", "BCHEUR");
                Console.WriteLine("------------------- {0} -------------------", DateTime.Now);
                foreach (TickerInformation tickerInformation in tickerInformations)
                {
                    Console.WriteLine("{0}: Bid:{1}, Ask:{2}", tickerInformation.PairName, tickerInformation.Bid.Price,
                        tickerInformation.Ask.Price);
                }
                Console.WriteLine();
                Thread.Sleep(300000);
            }
        }
    }
}